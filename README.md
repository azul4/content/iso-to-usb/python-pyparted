# python-pyparted

Python module for GNU parted

**Required by mintstick**

<br><br>

Do not compile in chroot

<br><br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/iso-to-usb/python-pyparted.git
```
